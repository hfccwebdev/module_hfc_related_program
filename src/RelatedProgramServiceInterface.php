<?php

namespace Drupal\hfc_related_program;

use Drupal\Core\Entity\EntityInterface;

/**
 * Defines the Related Program Connector Service Interface.
 */
interface RelatedProgramServiceInterface {

  /**
   * Set values for Related Program Connector based on source node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The source node.
   *
   * @return mixedarray
   *   An array of values.
   */
  public function getConnectorValues(EntityInterface $node);

  /**
   * Update hfc_related_program entity from node.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The source node.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  public function update(EntityInterface $node);

  /**
   * Update or remove hfc_related_program entity when node is deleted.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The source node.
   */
  public function nodeDelete(EntityInterface $node);

  /**
   * Check for broken related program entities.
   *
   * @param string[] $context
   *   Batch operations context variables.
   */
  public static function checkBroken(array &$context);

  /**
   * Bulk update all hfc_related_program entities by nodes.
   *
   * @param int[] $nids
   *   An array of nids to check.
   * @param string[] $context
   *   Batch operations context variables.
   */
  public static function bulkUpdate(array $nids, array &$context);

  /**
   * Callback when Catalog Bulk Update is finished.
   */
  public static function bulkUpdateFinishedCallback($success, $results, $operations);

}
