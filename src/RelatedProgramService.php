<?php

namespace Drupal\hfc_related_program;

use Drupal\Component\Utility\Html;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\hfc_catalog_helper\CatalogUtilitiesInterface;
use Drupal\hfc_related_program\Entity\RelatedProgram;
use Drupal\node\Entity\Node;

/**
 * Defines the Related Program Connector Service.
 */
class RelatedProgramService implements RelatedProgramServiceInterface {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  /**
   * Drupal\Core\Database\Connection definition.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Drupal\hfc_catalog_helper\CatalogUtilitiesInterface definition.
   *
   * @var \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface
   */
  protected $helper;

  /**
   * Constructs a new AssessmentReportTools object.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\hfc_catalog_helper\CatalogUtilitiesInterface $hfc_catalog_helper
   *   The Catalog helper service.
   */
  public function __construct(
    Connection $database,
    CatalogUtilitiesInterface $hfc_catalog_helper
  ) {
    $this->database = $database;
    $this->helper = $hfc_catalog_helper;
  }

  /**
   * {@inheritdoc}
   */
  public function getConnectorValues(EntityInterface $node) {

    switch ($node->getType()) {
      case 'program_master':
        $title = $node->label();
        $proposal = $this->helper->getActiveProgramProposal($node->id());
        $master = $node;
        $supplemental = $this->helper->getSupplementalProgramInfo($node->id());
        $catalog = $this->helper->getCatalogProgram($node->id());
        $degree_type = $node->field_program_type->value;
        $program_status = $node->field_program_status->value;
        break;

      case 'pseudo_program':
        $title = $node->label();
        $proposal = $node;
        $master = $node;
        $supplemental = NULL;
        $catalog = $node;
        $degree_type = NULL;
        $program_status = NULL;
        break;

      case 'program_proposal':
        if ($master = $node->field_program_master->entity) {
          $title = $master->label();
          $proposal = $node;
          $catalog = $this->helper->getCatalogProgram($master->id());
          $supplemental = $this->helper->getSupplementalProgramInfo($master->id());
          $degree_type = $master->field_program_type->value;
          $program_status = $master->field_program_status->value;
        }
        else {
          $title = $node->label();
          $proposal = $node;
          $master = NULL;
          $supplemental = NULL;
          $catalog = NULL;
          $degree_type = $node->field_program_type->value;
          $program_status = FALSE;
        }
        break;

      case 'supplemental_program_info':
        if ($master = $node->field_program_master->entity) {
          $title = $master->label();
          $proposal = $this->helper->getActiveProgramProposal($master->id());
          $catalog = $this->helper->getCatalogProgram($master->id());
          $supplemental = $node;
          $degree_type = $master->field_program_type->value;
          $program_status = $master->field_program_status->value;
        }
        else {
          $message = 'Cannot create related program connector from @title.';
          $values = ['@title' => $node->label()];
          $this->messenger()->addWarning($this->t($message, $values));
          $this->getLogger('hfc_related_program')->warning($message, $values);
          return FALSE;
        }
        break;

      case 'catalog_program':
        if ($master = $node->field_program_master->entity) {
          $title = $master->label();
          $proposal = $this->helper->getActiveProgramProposal($master->id());
          $catalog = $node;
          $supplemental = $this->helper->getSupplementalProgramInfo($master->id());
          $degree_type = $master->field_program_type->value;
          $program_status = $master->field_program_status->value;
        }
        else {
          $message = 'Cannot create related program connector from catalog program @title.';
          $values = ['@title' => $node->label()];
          $this->messenger()->addWarning($this->t($message, $values));
          $this->getLogger('hfc_related_program')->warning($message, $values);
          return FALSE;
        }
        break;
    }

    $values = [
      'title' => $title,
      'proposal_nid' => !empty($proposal) ? $proposal->id() : NULL,
      'master_nid' => !empty($master) ? $master->id() : NULL,
      'supp_nid' => !empty($supplemental) ? $supplemental->id() : NULL,
      'catalog_nid' => !empty($catalog) ? $catalog->id() : NULL,
      'degree_type' => $degree_type,
      'program_status' => $program_status,
    ];

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function update(EntityInterface $node) {
    $fields = [
      'program_proposal' => 'proposal_nid',
      'program_master' => 'master_nid',
      'supplemental_program_info' => 'supp_nid',
      'catalog_program' => 'catalog_nid',
      'pseudo_program' => 'master_nid',
    ];
    switch ($node->getType()) {
      case 'program_master':
        if ($rlt_prg = RelatedProgram::lookup($fields[$node->getType()], $node->id())) {
          return $this->doUpdate($node, $rlt_prg);
        }
        elseif ($rqc = RelatedProgram::lookup('title', $node->label())) {
          return self::doUpdate($node, $rqc);
        }
        else {
          return $this->doCreate($node);
        }
      case 'pseudo_program';
        if ($rlt_prg = RelatedProgram::lookup($fields[$node->getType()], $node->id())) {
          return $this->doUpdate($node, $rlt_prg);
        }
        else {
          return $this->doCreate($node);
        }
      case 'program_proposal':
        if ($master = $node->field_program_master->target_id) {
          if ($rlt_prg = RelatedProgram::lookup('master_nid', $node->field_program_master->target_id)) {
            return $this->doUpdate($node, $rlt_prg);
          }
          else {
            if ($this->doCreate($node->field_program_master->entity)) {
              $rlt_prg = RelatedProgram::lookup('master_nid', $node->field_program_master->target_id);
              return $this->doUpdate($node, $rlt_prg);
            }
          }
        }
        else {
          if ($rlt_prg = RelatedProgram::lookup('proposal_nid', $node->id())) {
            return $this->doUpdate($node, $rlt_prg);
          }
          else {
            return $this->doCreate($node);
          }
        }
      case 'catalog_program':
        if ($master = $node->field_program_master->target_id) {
          if ($rlt_prg = RelatedProgram::lookup('master_nid', $node->field_program_master->target_id)) {
            return $this->doUpdate($node, $rlt_prg);
          }
          else {
            $this->messenger()->addMessage($this->t('Cannot create Related Program Connector from Catalog Program for %t (%n)',
              ['%t' => $node->label(), '%n' => $node->id()]
            ));
            return FALSE;
          }
        }
      case 'supplemental_program_info':
        if ($master = $node->field_program_master->target_id) {
          if ($rlt_prg = RelatedProgram::lookup('master_nid', $node->field_program_master->target_id)) {
            return $this->doUpdate($node, $rlt_prg);
          }
          else {
            $this->messenger()->addMessage($this->t('Cannot create Related Program Connector from Supplemental Program Info for %t (%n)',
              ['%t' => $node->label(), '%n' => $node->id()]
            ));
            return FALSE;
          }
        }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function nodeDelete(EntityInterface $node) {
    $fields = [
      'program_proposal' => 'proposal_nid',
      'program_master' => 'master_nid',
      'supplemental_program_info' => 'supp_nid',
      'catalog_program' => 'catalog_nid',
      'pseudo_program' => 'master_nid',
    ];
    $field = $fields[$node->getType()];
    if ($rlt_prg = RelatedProgram::lookup($field, $node->id())) {
      $rlt_prg->set($field, 0);
      // @todo This should check to see if the connector is used in other content before removal!
      if ($rlt_prg->getProposalNid() == 0 && $rlt_prg->getMasterNid() == 0 && $rlt_prg->getCatalogNid() == 0) {
        $rlt_prg->delete();
      }
      else {
        $rlt_prg->save();
      }
    }
  }

  /**
   * Create a new rlt_prg record.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  private function doCreate(EntityInterface $node) {

    if ($values = $this->getConnectorValues($node)) {
      $rlt_prg = RelatedProgram::create($values);
      return $rlt_prg->save();
    }
  }

  /**
   * Update an existing rlt_prg record.
   *
   * @param \Drupal\Core\Entity\EntityInterface $node
   *   The source node to read.
   * @param \Drupal\hfc_related_program\Entity\RelatedProgram $rlt_prg
   *   The rlt_prg entity to update.
   *
   * @return int
   *   Either SAVED_NEW or SAVED_UPDATED, depending on the operation performed.
   */
  private function doUpdate(EntityInterface $node, RelatedProgram $rlt_prg) {
    $values = $this->getConnectorValues($node);

    if (!empty($values)) {

      $changed = FALSE;

      if ($rlt_prg->label() !== $values['title']) {
        $rlt_prg->setName($values['title']);
        $changed = TRUE;
      }
      if ($rlt_prg->getProposalNid() !== $values['proposal_nid']) {
        $rlt_prg->setProposal($values['proposal_nid']);
        $changed = TRUE;
      }
      if ($rlt_prg->getMasterNid() !== $values['master_nid']) {
        $rlt_prg->setMaster($values['master_nid']);
        $changed = TRUE;
      }
      if ($rlt_prg->getSuppNid() !== $values['supp_nid']) {
        $rlt_prg->setSupp($values['supp_nid']);
        $changed = TRUE;
      }
      if ($rlt_prg->getCatalogNid() !== $values['catalog_nid']) {
        $rlt_prg->setCatalog($values['catalog_nid']);
        $changed = TRUE;
      }
      if ($rlt_prg->getDegreeType() !== $values['degree_type']) {
        $rlt_prg->setDegreeType($values['degree_type']);
        $changed = TRUE;
      }
      if ($rlt_prg->getProgramStatus() !== $values['program_status']) {
        $rlt_prg->setProgramStatus($values['program_status']);
        $changed = TRUE;
      }

      if ($changed) {
        return $rlt_prg->save();
      }
      else {
        return SAVED_UPDATED;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function checkBroken(&$context) {
    $broken = \Drupal::database()->query("
      SELECT * FROM {related_program} WHERE
      (master_nid <> 0 AND master_nid NOT IN (SELECT nid FROM {node} WHERE type = 'program_master' OR type = 'pseudo_program')) OR
      (master_nid = 0 AND proposal_nid NOT IN (SELECT nid FROM {node} WHERE type = 'program_proposal')) ORDER BY title
    ")->fetchAll();

    foreach ($broken as $item) {
      $entity = RelatedProgram::load($item->id);
      if (!empty($entity->getProposalNid())) {
        // Clear master and catalog if proposal is still valid.
        // @todo Make sure proposal field_program_master is not set.
        $message = 'Clearing invalid Program Master from Related Program Connector @id: @title';
        $values = ['@id' => $entity->id(), '@title' => $entity->label()];
        \Drupal::messenger()->addWarning(t($message, $values));
        \Drupal::logger('hfc_related_program')->error($message, $values);
        $entity->setMaster(0);
        $entity->setCatalog(0);
        $entity->save();
      }
      else {
        // @todo This should check to see if the connector is used in a program before removal!
        $message = 'Removing invalid Related Program Connector @id: @title';
        $values = ['@id' => $entity->id(), '@title' => $entity->label()];
        \Drupal::messenger()->addError(t($message, $values));
        \Drupal::logger('hfc_related_program')->error($message, $values);
        $entity->delete();
      }
    }
    $context['message'] = 'Fixing broken connectors.';
    $context['results'][] = t('Found and repaired @count broken connectors.', ['@count' => count($broken)]);
  }

  /**
   * {@inheritdoc}
   */
  public static function bulkUpdate($nids, &$context) {

    if (empty($context['sandbox'])) {
      $context['sandbox']['progress'] = 0;
      $context['sandbox']['max'] = count($nids);
      $context['sandbox']['fail_count'] = 0;
    }

    // Batch cycle limit.
    $limit = 10;

    $slice = array_slice($nids, $context['sandbox']['progress'], $limit);
    $service = \Drupal::service('hfc_related_program_service');
    foreach ($slice as $nid) {
      $node = Node::load($nid);
      if ($service->update($node)) {
        $result = " - success: ($nid)";
      }
      else {
        \Drupal::messenger()->addError(t('@title - Update Failed', ['@title' => $node->label()]));
        $result = " - failed";
      }
      $context['sandbox']['progress']++;
      $context['sandbox']['current_id'] = $node->id();
      $context['message'] = Html::escape($node->label());
      $context['results'][] = $node->id() . ' : ' . Html::escape($node->label()) . $result;
      usleep(50000);
    }

    // Inform the batch engine that we are not finished,
    // and provide an estimation of the completion level we reached.
    if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
      $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function bulkUpdateFinishedCallback($success, $results, $operations) {
    // The 'success' parameter means no fatal PHP errors were detected. All
    // other error management should be handled using 'results'.
    if ($success) {
      $message = 'Process completed successfully.';
    }
    else {
      $message = 'Finished with an error.';
    }
    \Drupal::messenger()->addMessage(t($message));
  }

}
