<?php

namespace Drupal\hfc_related_program\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\hfc_related_program\RelatedProgramInterface;

/**
 * Defines the Related Program Connector entity.
 *
 * @ingroup hfc_related_program
 *
 * @ContentEntityType(
 *   id = "related_program",
 *   label = @Translation("Related Program Connector"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\hfc_related_program\RelatedProgramListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\hfc_related_program\Form\RelatedProgramForm",
 *       "add" = "Drupal\hfc_related_program\Form\RelatedProgramForm",
 *       "edit" = "Drupal\hfc_related_program\Form\RelatedProgramForm",
 *       "delete" = "Drupal\hfc_related_program\Form\RelatedProgramDeleteForm",
 *     },
 *     "access" = "Drupal\hfc_related_program\RelatedProgramAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\hfc_related_program\RelatedProgramHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "related_program",
 *   admin_permission = "administer related program",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *   },
 *   links = {
 *     "canonical" = "/related-program/{related_program}",
 *     "add-form" = "/related-program/add",
 *     "edit-form" = "/related-program/{related_program}/edit",
 *     "delete-form" = "/related-program/{related_program}/delete",
 *   },
 *   field_ui_base_route = "related_program.settings"
 * )
 */
class RelatedProgram extends ContentEntityBase implements RelatedProgramInterface {

  /**
   * {@inheritdoc}
   */
  public static function lookup($field, $nid) {
    $query = \Drupal::entityQuery('related_program')->condition($field, $nid);
    $ids = $query->execute();
    if (!empty($ids)) {
      return self::load(reset($ids));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('title', $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getProposal() {
    if (is_object($this->get('proposal_nid')->entity)) {
      return $this->get('proposal_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getProposalNid() {
    if (is_object($this->get('proposal_nid')->entity)) {
      return $this->get('proposal_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setProposal($nid) {
    $this->set('proposal_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getMaster() {
    if (is_object($this->get('master_nid')->entity)) {
      return $this->get('master_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMasterNid() {
    if (is_object($this->get('master_nid')->entity)) {
      return $this->get('master_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setMaster($nid) {
    $this->set('master_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getSupp() {
    if (is_object($this->get('supp_nid')->entity)) {
      return $this->get('supp_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getSuppNid() {
    if (is_object($this->get('supp_nid')->entity)) {
      return $this->get('supp_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setSupp($nid) {
    $this->set('supp_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getCatalog() {
    if (is_object($this->get('catalog_nid')->entity)) {
      return $this->get('catalog_nid')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCatalogNid() {
    if (is_object($this->get('catalog_nid')->entity)) {
      return $this->get('catalog_nid')->entity->id();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setCatalog($nid) {
    $this->set('catalog_nid', $nid);
  }

  /**
   * {@inheritdoc}
   */
  public function getDegreeType() {
    if (is_object($this->get('degree_type')->entity)) {
      return $this->get('degree_type')->entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDegreeType($degree_type) {
    $this->set('degree_type', $degree_type);
  }

  /**
   * {@inheritdoc}
   */
  public function getProgramStatus() {
    return $this->program_status->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setProgramStatus($program_status) {
    $this->set('program_status', $program_status);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Related Program Connector entity.'))
      ->setReadOnly(TRUE);

    $fields['proposal_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Current Proposal'))
      ->setDescription(t('Stores the node ID of currently active proposal for the program, if applicable.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['program_proposal' => 'program_proposal']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['master_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Program Master'))
      ->setDescription(t('Stores the node ID of the Program Master.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['program_master' => 'program_master']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['supp_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Supplemental Program Info'))
      ->setDescription(t('Stores the node ID of the Supplemental Program Info.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['supplemental_program_info' => 'supplemental_program_info']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['catalog_nid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Catalog Program'))
      ->setDescription(t('Stores the node ID of the catalog entry for this program.'))
      ->setSetting('target_type', 'node')
      ->setSetting('max_length', 7)
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['catalog_program' => 'catalog_program']])
      ->setDefaultValue('')
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'default',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The name of the Related Program Connector entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['degree_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Degree Type'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['program_status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Program Status'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => -1,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -1,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
