<?php

namespace Drupal\hfc_related_program\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Related Program Connector entities.
 */
class RelatedProgramViewsData extends EntityViewsData implements EntityViewsDataInterface {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['related_program']['table']['base'] = [
      'field' => 'id',
      'title' => $this->t('Related Program Connector'),
      'help' => $this->t('The Related Program Connector ID.'),
    ];

    return $data;
  }

}
