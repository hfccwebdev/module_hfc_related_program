<?php

namespace Drupal\hfc_related_program\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;

/**
 * Plugin implementation for Related Program Connector field formatter.
 *
 * @FieldFormatter(
 *   id = "hfc_related_program_field_formatter",
 *   label = @Translation("Related Program Connector"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class RelatedProgramFieldFormatter extends EntityReferenceFormatterBase {

  use LoggerChannelTrait;

  /**
   * Determine which related node to link.
   */
  protected static function programLinkModes() {
    return [
      'program_master' => t('Program Master (or Proposal)'),
      'catalog_program' => t('Catalog Program (or Proposal)'),
      'catalog_archive' => t('Catalog Archive'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'view_mode' => 'program_master',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['view_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Program link destination'),
      '#options' => $this->programLinkModes(),
      '#default_value' => $this->getSetting('view_mode'),
      '#description' => $this->t('Select which related program entity to display with this field.<br><em>(Automatically includes all pseudo-programs.)</em>'),
      '#weight' => 0,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if ($mode = $this->getSetting('view_mode')) {
      $mode = !empty($this->programLinkModes()[$mode]) ? $this->programLinkModes()[$mode] : $mode;
      $summary[] = $this->t('Program Link: %m', ['%m' => $mode]);
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   *
   * @see EntityReferenceFormatterBase::getEntitiesToView()
   */
  public function viewElements(FieldItemListInterface $items, $langcode = NULL) {
    $elements = [];

    $view_mode = $this->getSetting('view_mode');

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {
      $label = $entity->label();

      $node = $this->getTargetContent($entity);

      if (isset($node) && $node->access() && !$entity->isNew()) {
        if ($view_mode == 'catalog_archive') {
          $code = !empty($node->field_program_code->value)
            ? mb_strtolower($node->field_program_code->value)
            : 'ERROR';
          $elements[$delta] = [
            '#markup' => $this->t(
              '<a href="../programs/@code">@label</a>',
              ['@code' => $code, '@label' => $label]
            ),
          ];
        }
        else {
          $uri = $node->toUrl();
          $elements[$delta] = [
            '#type' => 'link',
            '#title' => $label,
            '#url' => $uri,
            '#options' => $uri->getOptions(),
          ];
        }
        if (!empty($items[$delta]->_attributes)) {
          $elements[$delta]['#options'] += ['attributes' => []];
          $elements[$delta]['#options']['attributes'] += $items[$delta]->_attributes;
          // Unset field item attributes since they have been included in the
          // formatter output and shouldn't be rendered in the field template.
          unset($items[$delta]->_attributes);
        }
      }
      else {
        $elements[$delta] = ['#plain_text' => $label];
      }
      $elements[$delta]['#cache']['max-age'] = 0;
      $elements[$delta]['#cache']['tags'] = $entity->getCacheTags();
    }

    return $elements;
  }

  /**
   * Get the target URI.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The required course connector entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The target node, if found.
   */
  private function getTargetContent(EntityInterface $entity) {
    $view_mode = $this->getSetting('view_mode');

    switch ($view_mode) {
      case 'program_master':
        $node = $entity->getMaster();
        if (empty($node)) {
          $node = $entity->getProposal();
        }
        break;

      case 'catalog_program':
      case 'catalog_archive':
        $node = $entity->getCatalog();
        if (empty($node)) {
          $node = $entity->getProposal();
        }
        break;

    }

    if (!empty($node)) {
      return $node;
    }
    else {
      $this->messenger()->addError($this->t('Error: Could not load details for related program @t', ['@t' => $entity->label()]));
      $this->getLogger('hfc_related_program')->error('Could not load details for related program @t', ['@t' => $entity->label()]);
      return NULL;
    }
  }

}
