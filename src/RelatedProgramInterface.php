<?php

namespace Drupal\hfc_related_program;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Related Program Connector entities.
 *
 * @ingroup hfc_related_program
 */
interface RelatedProgramInterface extends ContentEntityInterface {

  /**
   * Get hfc_related_program entity by Node ID.
   *
   * @param string $field
   *   Rlt_prg field to match.
   * @param int $nid
   *   Node ID to search.
   *
   * @return RelatedProgramInterface
   *   The selected related program connector
   */
  public static function lookup($field, $nid);

  /**
   * Gets the Related Program Connector name.
   *
   * @return string
   *   Name of the Related Program Connector.
   */
  public function getName();

  /**
   * Sets the Related Program Connector name.
   *
   * @param string $name
   *   The Related Program Connector name.
   */
  public function setName($name);

  /**
   * Gets the current Program Proposal.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Program Proposal node.
   */
  public function getProposal();

  /**
   * Gets the current Program Proposal Node ID.
   *
   * @return int
   *   The current Program Proposal nid.
   */
  public function getProposalNid();

  /**
   * Sets the Program Proposal Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setProposal($nid);

  /**
   * Gets the current Program Master.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Program Master node.
   */
  public function getMaster();

  /**
   * Gets the current Program Master Node ID.
   *
   * @return int
   *   The current Program Master nid.
   */
  public function getMasterNid();

  /**
   * Sets the Program Master Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setMaster($nid);

  /**
   * Gets the current Supplemental Program Info.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Supplemental Program Info node.
   */
  public function getSupp();

  /**
   * Gets the current Supplemental Program Info Node ID.
   *
   * @return int
   *   The current Supplemental Program Info nid.
   */
  public function getSuppNid();

  /**
   * Sets the Supplemental Program Info Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setSupp($nid);

  /**
   * Gets the current Catalog Program.
   *
   * @return \Drupal\node\NodeInterface
   *   The current Catalog Program node.
   */
  public function getCatalog();

  /**
   * Gets the current Catalog Program Node ID.
   *
   * @return int
   *   The current Catalog Program nid.
   */
  public function getCatalogNid();

  /**
   * Sets the Catalog Program Node ID.
   *
   * @param int $nid
   *   The node id.
   */
  public function setCatalog($nid);

}
