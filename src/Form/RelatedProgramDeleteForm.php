<?php

namespace Drupal\hfc_related_program\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Related Program Connector entities.
 *
 * @ingroup hfc_related_program
 */
class RelatedProgramDeleteForm extends ContentEntityDeleteForm {

}
