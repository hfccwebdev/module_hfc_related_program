<?php

namespace Drupal\hfc_related_program\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for Related Program Connector edit forms.
 *
 * @ingroup hfc_related_program
 */
class RelatedProgramForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\hfc_related_program\Entity\RelatedProgram $entity */
    $form = parent::buildForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->entity;
    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Related Program Connector.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Related Program Connector.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('entity.related_program.canonical', ['related_program' => $entity->id()]);
  }

}
