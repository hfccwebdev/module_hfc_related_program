<?php

namespace Drupal\hfc_related_program\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class the form for the bulk update for Related Programs.
 *
 * Form for bulk updates.
 */
class RelatedProgramBulkUpdateForm extends FormBase {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * Manager for Entity type.
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      // Load the services required to construct this class.
      $container->get('entity_type.manager')
    );
  }

  /**
   * Class constructor.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'related_program_bulk_update_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['message'] = [
      '#prefix' => '<p>',
      '#markup' => $this->t('This process will update all Related Program Connectors.'),
      '#suffix' => '</p>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update Connectors'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $nids = [];

    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE);
    $types = $query->orConditionGroup()
      ->condition('type', 'program_master')
      ->condition('type', 'pseudo_program');
    $masters = $query
      ->condition($types)
      ->sort('title')
      ->execute();

    foreach ($masters as $nid) {
      $nids[] = $nid;
    }

    $proposals = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'program_proposal')
      ->notExists('field_program_master')
      ->condition('field_proposal_processed', 0)
      ->sort('title')
      ->execute();

    foreach ($proposals as $nid) {
      $nids[] = $nid;
    }

    $batch = [
      'title' => $this->t('Updating Related Program Connectors...'),
      'operations' => [
        ['\Drupal\hfc_related_program\RelatedProgramService::checkBroken', []],
        ['\Drupal\hfc_related_program\RelatedProgramService::bulkUpdate',
          [$nids],
        ],
      ],
      'finished' => '\Drupal\hfc_related_program\RelatedProgramService::bulkUpdateFinishedCallback',
    ];
    batch_set($batch);
  }

}
