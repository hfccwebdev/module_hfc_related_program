<?php

/**
 * @file
 * Contains related_program.page.inc.
 *
 * Page callback for Related Program Connector entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Related Program Connector templates.
 *
 * Default template: related_program.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_related_program(array &$variables) {

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
